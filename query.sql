CREATE DATABASE QLSV;

USE QLSV;

CREATE TABLE DMKHOA(
	MaKH  VARCHAR(6) primary key,
	TenKhoa VARCHAR(30)
);

CREATE TABLE SINHVIEN(
	MaSV VARCHAR(6) PRIMARY KEY,
    HoSV VARCHAR(30),
    TenSV VARCHAR(15),
    GioiTinh CHAR,
    NgaySinh DateTime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int
);

select * from SINHVIEN as sv,DMKHOA as dmk 
where sv.MaKH = dmk.MaKH and dmk.TenKhoa='Công nghệ thông tin';